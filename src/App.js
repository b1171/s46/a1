import './App.css';

import {Fragment} from 'react';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';

function App() {

  return(
    <Fragment>
      <AppNavbar />
      <Home />
    </Fragment>
  )
}

export default App;

