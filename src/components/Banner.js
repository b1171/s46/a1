
export default function Banner() {

	return (
		<div className="container-fluid">
			<div className="row justify-content-center m-5">
				<div className="col-10 col-md-9">

					<div className="jumbotron">
						<h1>Welcome to React Booking</h1>
						<p>Opportunities for everyone, everywhere!</p>
						<button className="btn btn-primary">Enroll</button>
					</div>
				</div>	
			</div>
		</div>
	)
}