
import {useState} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function CourseCard({courseProp}) {
	
	const {name, description, price} = courseProp;
	
	//use the state hook for this component to be able to store its state
	//States are used to keep track of information related to individual components
	
	/*
	Syntax:
		const [getter, setter] = useState(initialGetterValue)
	*/

	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(10);

	function enroll() {
		if(seats <= 0) {
			alert("No more seats available");
		} else {
			setCount(count + 1);
			setSeats(seats - 1);
		}

		console.log(count + " " + seats)
	}

	return (

		<Container fluid className="my-4">
			<Row className="justify-content-center">
				<Col xs={10} lg={9}>					
					<Card className="cardHighlights">
					  <Card.Body>
					    <Card.Title>{name}</Card.Title>
					    
					    <Card.Subtitle>Description:</Card.Subtitle>
					    <Card.Text>
					    	{description}
					    </Card.Text>
					    
					    <Card.Subtitle>Price:</Card.Subtitle>
					    <Card.Text>
					    	{price}
					    </Card.Text>

					    <Card.Text>
					    	Enrollees: {count} 
					    </Card.Text>

					    <Button variant="primary" onClick={enroll}>Enroll</Button>
					  </Card.Body>
					</Card>
				</Col>

			</Row>
		</Container>

	)
}

//check if coursecard component is getting the correct prop types
//proptypes are used for validataing information passed to a
//component and is a tool normallyused to help developers ensure the
//correct information is passed from one component to the next

CourseCard.propTypes = {
	//shape method => is used to check if a prop object conforms to a
	//specific "shape"

	courseProp: PropTypes.shape({
		//define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}