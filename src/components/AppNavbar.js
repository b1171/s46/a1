
import {Navbar, Nav} from 'react-bootstrap';

export default function AppNavbar() {

	return (
		<Navbar bg="primary" expand="lg" className="p-2">
			<Navbar.Brand href="#">React Booking</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="me-auto">
					<Nav.Link>Home</Nav.Link>
					<Nav.Link>Courses</Nav.Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}

